<?php

class Dexm_Best_Block_Bestseller extends Mage_Catalog_Block_Product_List {

    protected function _getProductCollection()
    {
        if (is_null($this->_productCollection)) {
			
			
			$storeId = Mage::app()->getStore()->getId();
            $collection = Mage::getResourceModel('reports/product_collection')
            ->addOrderedQty()
            ->addAttributeToSelect('*')
            ->setStoreId($storeId)
            ->setOrder('ordered_qty', 'desc');
            
            Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
            Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);
            $this->_productCollection = $collection;
 
        }
        return $this->_productCollection;
    }
	
}
