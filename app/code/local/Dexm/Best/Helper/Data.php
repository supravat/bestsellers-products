<?php


Class Dexm_Best_Helper_Data extends Mage_Core_Helper_Abstract {
	
	
	public function getBestsellersCollection() {
    $productCount = 5;
    $storeId = Mage::app()->getStore()->getId();

    $productsBestSellerMens = Mage::getResourceModel('reports/product_collection')
            ->addOrderedQty()
            ->addAttributeToSelect('*')
            ->setStoreId($storeId)
            ->setOrder('ordered_qty', 'desc');

    return $productsBestSellerMens->getData();
}
}
	
